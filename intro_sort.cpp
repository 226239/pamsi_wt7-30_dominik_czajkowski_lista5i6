#include "intro_sort.h"
#include "quick_sort.h"
#include <math.h>

void swap (int *tb, long i, long j)
{
  int temp;
  temp=tb[i];
  tb[i]=tb[j];
  tb[j]=temp;
}

void Heapify (int *tb, long i, long n)
{
  long j;
  while (i<=n/2)
  {
    j=2*i;
    if (j+1<=n && tb[j+1]>tb[j])
      j=j+1;
    if (tb[i]<tb[j])
      swap(tb,i,j);
    else break;
    i=j;
  }
}

void Heap_Sort (int *tb, long n)
{
  long i;
  for (i=n/2; i>0; --i)
    Heapify(tb-1,i,n);
  for (i=n-1; i>0; --i)
  {
    swap(tb,0,i);
    Heapify(tb-1,1,i);
  }
}



void MedianOfThree (int *Array, long &L, long &R)
{
  if (Array[++L-1]>Array[--R])
    swap(Array,L-1,R);
  if (Array[L-1]>Array[R/2])
    swap(Array,L-1,R/2);
  if (Array[R/2]>Array[R])
    swap(Array,R/2,R);
  swap(Array,R/2,R-1);
}

long Partition (int *Array, long L, long R)
{
  long i, j;
  if (R>=3)
    MedianOfThree(Array,L,R);
  for (i=L, j=R-2; ; )
  {
    for ( ; Array[i]<Array[R-1]; ++i);
    for ( ; j>=L && Array[j]>Array[R-1]; --j);
    if (i<j)
      swap(Array,i++,j--);
    else break;
  }
  swap(Array,i,R-1);
  return i;
}
void IntroSort (int *tb, long N, int M)
{
  long i;
  if (M<=0)
  {
    Heap_Sort(tb,N);
    return;
  }
  i=Partition(tb,0,N);
  if (i>9)
    IntroSort(tb,i,M-1);
  if (N-1-i>9)
    IntroSort(tb+i+1,N-1-i,M-1);
}

void Introspective_Sort (int *tb, long N)
{
  IntroSort(tb,N,(int)floor(2*log(N)/M_LN2));
  quick_sort(tb,N);
}
