#include "merge_sort.h"

int *pom; //tablica pomocnicza, potrzebna przy scalaniu

//scalenie posortowanych podtablic

void scal(int tab[], long lewy, long srodek, long prawy)
{
    long i = lewy, j = srodek + 1;

    //kopiujemy lewą i prawą część tablicy do tablicy pomocniczej
    for(long i = lewy;i<=prawy; i++)
      pom[i] = tab[i];

    //scalenie dwóch podtablic pomocniczych i zapisanie ich
    //we własciwej tablicy
    for(long k=lewy;k<=prawy;k++)
    if(i<=srodek)
      if(j <= prawy)
           if(pom[j]<pom[i])
               tab[k] = pom[j++];
           else
               tab[k] = pom[i++];
      else
          tab[k] = pom[i++];
    else
        tab[k] = pom[j++];
}

void rec_m_sort(int *tab,long lewy, long prawy)
{
    //gdy mamy jeden element, to jest on już posortowany
    if(prawy<=lewy) return;

    //znajdujemy srodek podtablicy
    long srodek = (prawy+lewy)/2;

    //dzielimy tablice na częsć lewą i prawa
    rec_m_sort(tab, lewy, srodek);
    rec_m_sort(tab, srodek+1, prawy);

    //scalamy dwie już posortowane tablice
    scal(tab, lewy, srodek, prawy);
}

void merge_sort(int *tb,long n)
{
    pom = new int[n];
    rec_m_sort(tb,0,n-1);
}
