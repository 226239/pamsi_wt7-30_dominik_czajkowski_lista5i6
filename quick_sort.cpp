#include "quick_sort.h"

void qsort_req(int *tab, long lewy, long prawy)
{
  if(prawy <= lewy) return;

  long i = lewy - 1, j = prawy + 1,
  pivot = tab[(lewy+prawy)/2]; //wybieramy punkt odniesienia

  while(1)
  {
    //szukam elementu wiekszego lub rownego piwot stojacego
    //po prawej stronie wartosci pivot
    while(pivot>tab[++i]);

    //szukam elementu mniejszego lub rownego pivot stojacego
    //po lewej stronie wartosci pivot
    while(pivot<tab[--j]);

    //jesli liczniki sie nie minely to zamień elementy ze soba
    //stojace po niewlasciwej stronie elementu pivot
    if( i <= j)
    {
      //funkcja swap zamienia wartosciami tab[i] z tab[j]
      long buf;
        buf = tab[j];
        tab[j] = tab[i];
        tab[i] = buf;
    }
    else
      break;
  }

  if(j > lewy)
  qsort_req(tab, lewy, j);
  if(i < prawy)
  qsort_req(tab, i, prawy);

}

void quick_sort(int *t, long n)
{
    qsort_req(t,0,n-1);
}
