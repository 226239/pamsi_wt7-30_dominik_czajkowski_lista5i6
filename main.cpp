#include <iostream>
#include <ctime>
#include "merge_sort.h"
#include "quick_sort.h"
#include "intro_sort.h"

using namespace std;

void list(int *t, unsigned int size)
{
    for(unsigned int i=0;i<size;i++)
        cout<<t[i]<<" ";
    cout << endl;
}

void all_random(long s, double prc, int sort)
{
    for(int j=0; j<100;j++)
    {
    int tab[s];
    for(int i=0; i<s; i++)
    {
        if(prc != -1)
        {
        if(i < s*prc) tab[i] = i;
        else
         tab[i] = rand() + s*prc;
        }
        else
            tab[i] = s - i;
    }
    if(sort == 0)
         merge_sort(tab,s);
    if(sort == 1)
        quick_sort(tab, s);
    if(sort == 2)
        Introspective_Sort(tab,s);

    }

}



double obliczSekundy( clock_t czas )
{
    return static_cast < double >( czas ) / CLOCKS_PER_SEC;
}

void merge_sort_test(long s, double prc, int sort)
{
    cout << "100 arrays with "<< s << " elements" << endl;
    cout << "I'm sort, please wait..."<<endl;
    clock_t t1, t2;
    t1 = t2 = clock();
    all_random(s, prc, sort);
    while(t1 == t2)
        t2 = clock();
    cout << "Operation time: "<< obliczSekundy(t2-t1)<< "s" << endl ;
}

void sort_complex_test(double prc, int sort)
{
    if(prc != -1) cout << "**********"<< prc*100 << "% sorted arrays**********" << endl;
    else cout <<  "**********100% unsorted arrays**********" << endl;
    merge_sort_test(10000,prc, sort);
    merge_sort_test(25000,prc, sort);
    merge_sort_test(100000,prc, sort);
    merge_sort_test(500000,prc, sort);
    merge_sort_test(1000000,prc, sort);
}

int main()
{
//    int *tablica,
//    n; //liczba elementów tablicy

//    cin>>n;
//    tablica = new int[n]; //przydzielenie pamięci na tablicę liczb

//    //wczytanie elementów tablicy
//    for(int i=0;i<n;i++)
//      tablica[i] = rand()%(n+100);
//    cout << "Before sort:";
//    list(tablica,n);


//    //sortowanie wczytanej tablicy
//    Introspective_Sort(tablica,n);
//    cout <<"After sort:";
//    list(tablica,n);
//    cout << "******Merge sort******" << endl;
//    sort_complex_test(0,0);
//    sort_complex_test(0.25,0);
//    sort_complex_test(0.5,0);
//    sort_complex_test(0.75,0);
//    sort_complex_test(0.95,0);
//    sort_complex_test(0.99,0);
//    sort_complex_test(0.997,0);
//    sort_complex_test(-1, 0);

//    cout << "******Quick sort******" << endl;
//    sort_complex_test(0,1);
//    sort_complex_test(0.25,1);
//    sort_complex_test(0.5,1);
//    sort_complex_test(0.75,1);
//    sort_complex_test(0.95,1);
//    sort_complex_test(0.99,1);
//    sort_complex_test(0.997,1);
//    sort_complex_test(-1, 1);

    cout << "******Introspective sort******" << endl;
 //   sort_complex_test(0,2);
//    sort_complex_test(0.30,2);
//    sort_complex_test(0.5,2);
//    sort_complex_test(0.75,2);
//    sort_complex_test(0.95,2);
//    sort_complex_test(0.99,2);
//    sort_complex_test(0.997,2);
//    sort_complex_test(-1, 2);
merge_sort_test(250000, 0.30, 2);
merge_sort_test(250000, 0.30, 1);

}
